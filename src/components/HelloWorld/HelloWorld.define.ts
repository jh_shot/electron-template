import { string } from 'vue-types'

export const helloWordProps = () => ({
  msg: string().isRequired
})
