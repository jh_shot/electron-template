import { defineComponent, reactive, toRefs } from 'vue'

import electronLogo from '@/assets/images/electron.svg'
import viteLogo from '@/assets/images/vite.svg'
import vueLogo from '@/assets/images/vue.svg'

import { helloWordProps } from './HelloWorld.define'
import styles from './HelloWorld.module.css'

export default defineComponent({
  props: helloWordProps(),
  setup() {
    const state = reactive({
      count: 0
    })

    function handleClick() {
      state.count++
    }

    return {
      ...toRefs(state),
      handleClick
    }
  },
  render() {
    return (
      <>
        <div>
          <a href="https://www.electronjs.org/" target="_blank">
            <img
              src={electronLogo}
              class={[styles.logo, 'electron']}
              alt="Electron logo"
            />
          </a>
          <a href="https://vitejs.dev/" target="_blank">
            <img src={viteLogo} class={[styles.logo, 'vite']} alt="Vite logo" />
          </a>
          <a href="https://vuejs.org/" target="_blank">
            <img src={vueLogo} class={[styles.logo, 'vue']} alt="Vue logo" />
          </a>
        </div>
        <h1>{this.msg}</h1>
        <div>
          <button class={styles.countBtn} onClick={this.handleClick}>
            count is {this.count}
          </button>
          <p>
            Edit
            <code class={styles.code}>
              components/HelloWorld/HelloWord.tsx
            </code>{' '}
            to test HMR
          </p>
        </div>
        <p>
          Check out
          <a
            class={styles.link}
            href="https://vuejs.org/guide/quick-start.html#local"
            target="_blank"
          >
            create-vue
          </a>
          , the official Vue + Vite starter
        </p>
        <p>
          Install
          <a
            class={styles.link}
            href="https://github.com/johnsoncodehk/volar"
            target="_blank"
          >
            Volar
          </a>
          in your IDE for a better DX
        </p>
        <p class={styles.readTheDocs}>
          Click on the Vite and Vue logos to learn more
        </p>
        <div class={styles.staticFiles}>
          Place static files into the <code class={styles.code}>/public</code>{' '}
          folder
          <img class={styles.staticLogo} src="/logo.svg" alt="Logo" />
        </div>
      </>
    )
  }
})
