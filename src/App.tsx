import { defineComponent } from 'vue'

import { HelloWorld } from './components'

export default defineComponent({
  name: 'App',
  render() {
    return (
      <div>
        <HelloWorld msg="Electron + Vite + Vue" />
      </div>
    )
  }
})
